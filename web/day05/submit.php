<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Student</title>
</head>
<body>
    <?php
        $data = $_SESSION["data"];
        $genderArray = array("1" => "Nam", "2" => "Nữ");
        $departments = array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
    ?>

    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <div class="center">
        <div class="container">            
            <form action="" method="POST" >
                <div class="wrapper">
                    <div class="label-row label-row-submit name">
                        <button>Họ và tên</button>
                        <h5><?php echo $data["name"] ?></h5>
                    </div>
                    <div class="label-row label-row-submit gender">
                        <button>Giới tính</button>
                        <h5><?php echo $genderArray[$data["gender"]] ?></h5>
                    </div>
                    <div class="label-row label-row-submit department">
                        <button>Phân khoa</button>
                        <h5><?php echo $departments[$data["department"]] ?></h5>
                    </div>
                    <div class="label-row label-row-submit date">
                        <button>Ngày sinh</button>
                        <h5><?php echo $data["date"] ?></h5>
                    </div>
                    <div class="label-row label-row-submit address">
                        <button>Địa chỉ</button>
                        <h5><?php echo $data["address"] ?></h5>
                    </div>
                    <div class="label-row label-row-submit image-submit">
                        <button>Hình ảnh</button>
                        <?php 
                        if (!empty($data["imageFile"])){
                        ?>
                            <img src="<?php echo $data["imageFile"] ?>" alt="">
                        <?php
                        }
                        ?>
                    </div>
                    <div class="register">
                        <input type="submit" value="Xác nhận">
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
<style><?php include './style.css' ?></style>
</html>